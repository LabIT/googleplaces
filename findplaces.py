#!/usr/bin/env python3

import requests
import json
import time

API_KEY='<<Omitted>>'
PLACES_API='https://maps.googleapis.com/maps/api/place'
SEARCH_API='nearbysearch'
DETAIL_API='details'
FORMAT='json'
FIELDS='name,formatted_address,formatted_phone_number,website,business_status'
SUBURB_FILE='postcodes.json'
OUTPUT_FILE='output.json'
radius='1500'
establishment_type='dentist'
place_id = []

def make_places_search(suburb_lat, suburb_lon, radius, establishment_type, next_token=''):
    try: 
        response = requests.get(f"{PLACES_API}/{SEARCH_API}/{FORMAT}?location={suburb_lat},{suburb_lon}&radius={radius}&type={establishment_type}&key={API_KEY}&pagetoken={next_token}")
        return response.json()

    except requests.exceptions.TooManyRedirects as error:
           print(error)
    except requests.ConnectionError as error:
           print(error)
    except requests.Timeout as error:
           print(error)

def get_place_details(place_id):
    try:
        response = requests.get(f"{PLACES_API}/{DETAIL_API}/{FORMAT}?place_id={place_id}&fields={FIELDS}&key={API_KEY}")
        return response.json()
    except requests.exceptions.TooManyRedirects as error:
           print(error)
    except requests.ConnectionError as error:
           print(error)
    except requests.Timeout as error:
           print(error)

def load_places_data(data):
    for i in range(len(data["results"])):
        print('Appending following place_IDs to the list: ', data["results"][i]["place_id"])
        place_id.append(data["results"][i]["place_id"])

    

with open('{0}'.format(SUBURB_FILE),'r') as postcode:
    suburbs = json.load(postcode)
    
    for key in suburbs:
        
        suburb_postcode=suburbs[key][0][0]
        suburb_name=suburbs[key][0][1]
        suburb_state=suburbs[key][0][2]
        suburb_lat=suburbs[key][0][3]
        suburb_lon=suburbs[key][0][4]

        print('Working through suburb : ', suburb_name, 'with post code : ', suburb_postcode)
        
        print('calling places API for the first time on suburb')
        first_response = make_places_search(suburb_lat, suburb_lon, radius, establishment_type)

        if first_response["status"] == 'OK':
            load_places_data(first_response)

            if 'next_page_token' in first_response:
                print('First page token exists')
                
                time.sleep(2)
                second_response = make_places_search(suburb_lat, suburb_lon, radius, establishment_type, first_response['next_page_token'])
                load_places_data(second_response)

                if 'next_page_token' in second_response:
                    print('third page token exists')
                    time.sleep(2)
                    third_response = make_places_search(suburb_lat, suburb_lon, radius, establishment_type, second_response['next_page_token'])
                    
                    load_places_data(third_response)
                else:
                    print('suburb name ', suburb_name, 'with postcode', suburb_postcode, 'has been loaded')
            else:
                print('suburb name ', suburb_name, 'with postcode', suburb_postcode, 'has been loaded')


            print('suburb name ', suburb_name, 'with postcode', suburb_postcode, 'has been loaded') 

            print('Found ', len(place_id), 'records so far; writing them to the file ............')     

            for i in range(len(place_id)):
                place_detail = get_place_details(place_id[i])
                with open('{0}'.format(OUTPUT_FILE),'a') as outfile:
                    json.dump(place_detail["result"], outfile, sort_keys=True, indent=4)

            print('Total records found : ', len(place_id), 'Write to file Complete, please check output.txt for details')


        
        else:
            print('BAD REQUEST')


    







        

            
            
        
        
            
       


        









